#!/bin/ksh

# 'pharc' or 'phlog archiver' is a basic script to automatically archive phlog 
# posts on a yearly basis.
#
# Depends on 'mkphlog', written by octotep. Otherwise it is completely useless
# unless you have your posts in the form $(date +%m-%d-%y). You are warned!!!
#
# Suggestions:	
#		-Use it with 'cron'. Ideally on December 31 just before midnight:
# 		 55 23 31 12 *
# 		-To create a log: pharc 2>&1 >> pharc.log
#
# Copyright © 2013 Carlos Zuferri License GPLv3+: GNU
# GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.
#
# TODO Improve it (As usual)
#
#		* Add option to choose 'different' year of archive.
#
# Changelog:	
#		* Added option -a to specify current year of archive (redundant).
#		* Added -v (version information option).
#		* Added -h (help menu).
#		* Added -y (year of the archive).
#		* Help function (usage).
#		* Improve coding style.
#		* Leave a backup of gophermap > gophermap~ (in case things break).
#		* Test before echoing "You're done now. Thanks for using pharc."


# Writing functions.

usage ()
{
echo	"Usage:	
	pharc -v	(Prints version and exits.)
	pharc -h	(Prints help and exits.)
	pharc -y	(Prints year of archive and exits)
	pharc -a <YEAR>	(Allows to specify year of archive)" 
exit 0
}

# Adding options.

VERSION=3.0
ARCHIVE=$(date +'%Y')

while [ "$1" != "" ]
do
	case "$1" in
	-v|--version)	echo	$VERSION
	  		exit 0
			;;

	-h|--help)	usage
			exit 0
			;;

	-y|--year)	echo $(date +'%Y')
			exit 0
			;;

	-a|--archive)	shift
			ARCHIVE=$1
			;;

	*)	usage
		exit 1

	esac
	shift		
done

# Testing to see if the script can be run.

WORKDIR=$HOME/gopher/phlog

cd $HOME

if [ ! -d $WORKDIR ]
then
	echo "$(date) No working directory found: $WORKDIR does not exist."
	exit 1
fi

if [ -d $WORKDIR/$(date +'%Y') ]
then
	echo "$(date) Archive $(date +'%Y') already exists. Exiting..."
	exit 1
fi

if [ ! -d $WORKDIR/*-*-$(date +'%y') ]
then
	echo "$(date) No phlog posts to archive."
	exit 1
else
	echo ""
	echo "$(date) Archiving phlog posts..."
fi

# Creating target directory and moving files there.

cd $WORKDIR
mkdir $ARCHIVE
chmod 750 $ARCHIVE
mv *-*-$(date +'%y') $ARCHIVE

# Creating && editing gophermaps.

cp gophermap $ARCHIVE/gophermap~
cd $WORKDIR/$ARCHIVE
sed /1Archive.*/d < gophermap~ > gophermap
chmod 640 gophermap
rm gophermap~

# Updating for mkphlog v.0.2

cd $WORKDIR
mv gophermap gophermap~
sed /1/d < gophermap~ > gophermap
chmod 640 gophermap

DIRS=$(ls -d 20??)

for DIR in $DIRS
	do
		echo -e "1Archive $DIR\t$DIR" >> gophermap
	done
chmod 640 gophermap

sleep 2

if [ "$(ls -A $WORKDIR/$(date +'%Y'))" ]
then
	echo ""
	echo "$(date) You are done now. Thanks for using pharc!"
	echo ""
else
	echo ""
	echo "$(date) Maybe unsuccessful. Please check it out. If things"
	echo "broke badly there is a backup of your gophermap --> gophermap~"
	echo ""
fi

# "You can see the output of this script visiting my gopher hole at:"
# "<gopher://sdfeu.org/1/users/chals/>"

exit 0
