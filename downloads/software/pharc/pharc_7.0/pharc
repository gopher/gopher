#!/bin/sh

set -e

# Copyright © 2011-2022 Carlos Zuferri License GPLv3+: GNU
# GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.
#
# 'pharc' or 'phlog archiver' is a simple script to automatically archive phlog
# posts on a yearly basis. See wiki at <https://gitlab.com/pharc/pharc/wikis/home>
#
# Depends on 'mkphlog', otherwise, it is completely useless unless you have your
# posts in directories named in the form $(date +%m-%d-%y).'mkphlog' was written
# by Chris Yealy (octotep). The newest version can be found at:
#
# <gopher://sdf.org/1/users/octotep/dls/mkphlog>
# or
# <https://bitbucket.org/octotep/mkphlog/>
#
# Suggestions:
#		-Use it with 'cron'. Ideally on December 31st just before midnight:
# 		 55 23 31 12 *
#		-To change the working directory: export PHARC_WORKDIR="/path/to/phlog"
# 		-To change the log filename: export  PHARC_LOGFILE="/path/to/pharc.log"
#
# Limitations:
#
#	-pharc only works with the 21st century year range (2000-2099).
#
# Notes:
#
#	- pharc makes a backup of your gophermap -> gophermap~ in case things go wrong.
#
# TODO:
#		* Improve it (as usual).
#		* Drop debian package or .deb from git.
#		* Add pharc wiki to the gopherhole.
#
# Changelog:
#
#  * v.7.0
#
#   * Updating file permissions for gophernicus.
#
#  * v.6.0
#
#    * Sort Archives in reverse mode.
#
#  * v.5.0
#
#    * Add '-A|--all' option to archive all years at once.
#
#  * v.4.0
#
#    * Changed shebang to #!/bin/sh to make script more portable.
#    * Improved if[...]else statements to make script more robust.
#    * Added functionality to choose year of archive.
#    * Added functionality to specify several years in one go.
#    * Added '-l|--list' option to list unarchived phlog posts.
#    * Logging by default to "$HOME/pharc.log"
#    * Split code into functions.
#    * Added option '-d|--delete to delete the log file.
#
#  * v.3.0
#
#    * Added option -a to specify year of archive (Redundant).
#    * Added -v (version information option).
#    * Added -h (help menu).
#    * Added -y (year of the archive).
#    * Help function (usage).
#    * Improve coding style.
#    * Leave a backup of gophermap -> gophermap~ (in case things break).
#    * Test before echoing "You're done now. Thanks for using pharc."
#
#  * v.2.0
#
#    * Updating for mkphlog v.0.2
#    * Adding version number 2.0
#    * Adding license (2012)
#
#  * Initial release.
#

# Setting defaults

PHARC_VERSION="7.0"
PHARC_WORKDIR="${PHARC_WORKDIR:-"$HOME/gopher/phlog"}"
PHARC_ARCHIVE="${PHARC_ARCHIVE:-"$(date +'%Y')"}"
PHARC_LOGFILE="${PHARC_LOGFILE:-"$HOME/pharc.log"}"

# Writing functions.

Usage ()
{
echo	"Usage:
	pharc -a, --archive <YEAR> ... <YEAR> (Specify year or years to archive)
	pharc -A, --all			(Archives all unarchived years at once)
	pharc -d, --delete		(Deletes pharc's log file interactively)
	pharc -h, --help		(Prints help and exits)
	pharc -l, --list		(Lists unarchived phlog posts, if any, and exits)
	pharc -v, --version		(Prints version and exits)
	pharc -y, --year		(Prints year of archive and exits)

	* pharc's default working directory is "HOME/gopher/phlog"
	To change it: export PHARC_WORKDIR="/path/to/phlog"

	* pharc creates a logfile by default in "HOME/pharc.log"
	To change it: export PHARC_LOGFILE="/path/to/pharc.log"
	"
exit 0
}

Initial_test ()
{
# Testing to see if the script can be run.

if [ ! -d ${PHARC_WORKDIR} ]
then
	echo "$(date) No working directory found: ${PHARC_WORKDIR} does not exist." | tee -a ${PHARC_LOGFILE}
	exit 1
elif [ -d ${PHARC_WORKDIR}/${PHARC_ARCHIVE} ]
then
	echo "$(date) Archive ${PHARC_ARCHIVE} already exists. Exiting..." | tee -a ${PHARC_LOGFILE}
	exit 1
elif [ ! -d ${PHARC_WORKDIR}/*-*-${PHARC_ARCHIVE#20} ] > /dev/null 2>&1
then
	echo "$(date) No ${PHARC_ARCHIVE} phlog posts to archive." | tee -a ${PHARC_LOGFILE}
	exit 1
else
	echo "" | tee -a ${PHARC_LOGFILE}
	echo "$(date) Archiving ${PHARC_ARCHIVE} posts..." | tee -a ${PHARC_LOGFILE}
fi
}

Move_posts ()
{
# Creating target directory and moving files there.

cd ${PHARC_WORKDIR}
mkdir ${PHARC_ARCHIVE}
chmod 755 ${PHARC_ARCHIVE}
mv *-*-${PHARC_ARCHIVE#20} ${PHARC_ARCHIVE}
}

# Creating and editing gophermaps:

Gophermap_1 ()
{
# Archive's gophermap.

cp gophermap ${PHARC_ARCHIVE}/gophermap~
cd ${PHARC_WORKDIR}/${PHARC_ARCHIVE}
head -n 3 gophermap~ > gophermap
sed "/^1.*(*-*-${PHARC_ARCHIVE#20}$/!d" < gophermap~ >> gophermap
rm -f gophermap~
chmod 644 gophermap
}

Gophermap_2 ()
{
# Workdir's gophermap.

cd ${PHARC_WORKDIR}
mv gophermap gophermap~
sed -e "/1Archive.*/d" -e "/^1.*(*-*-${PHARC_ARCHIVE#20}$/d" < gophermap~ > gophermap
chmod 644 gophermap

DIRS=$(ls -d 20?? | sort -r)

for DIR in ${DIRS}
	do
		printf "1Archive $DIR\t$DIR\n" >> gophermap
	done
chmod 644 gophermap
}

Final_test ()
{
# Perform final tests.

if [ -d "${PHARC_ARCHIVE}" ] && [ "$(ls -A ${PHARC_ARCHIVE})" ]
then
	echo	"$(date) Done. Thanks for using pharc!" | tee -a ${PHARC_LOGFILE}
	echo "" | tee -a ${PHARC_LOGFILE}
else
	echo "" | tee -a ${PHARC_LOGFILE}
	echo	"$(date) Maybe unsuccessful!!! Please check it out. If things
			broke badly there is a backup of your gophermap -> gophermap~" | tee -a ${PHARC_LOGFILE}
fi
}

Archive_phlog ()
{
Initial_test
Move_posts
Gophermap_1
Gophermap_2
Final_test
}

# Adding options.

if [ "$1" = "" ]
then
    Archive_phlog
else
	case "$1" in

	-a|--archive)
			shift
            for PHARC_YEAR in "$@"
				do
                    PHARC_ARCHIVE="${PHARC_YEAR}"
                    Archive_phlog
                done
			exit 0
			;;

	-A|--all)
			shift
            for PHARC_DIR in $(cd ${PHARC_WORKDIR} && ls -d *-*-* | sed 's/^.*\(..\)$/\1/' | sort | uniq)
                do
					PHARC_YEAR=20$(echo $PHARC_DIR)
					PHARC_ARCHIVE="${PHARC_YEAR}"
					Archive_phlog
                done
			exit 0
			;;

	-d|--delete)
			rm -i ${PHARC_LOGFILE}
			;;

	-h|--help)
			Usage
			exit 0
			;;

	-l|--list)
			set +e
			cd ${PHARC_WORKDIR} && ls -d *-*-*/  > /dev/null 2>&1
			if [ "$?" -eq "0" ]
			then
				echo "There are $(ls -d *-*-*/ | wc -w) unarchived phlog posts:"
				ls -d *-*-*/ | sort
			else
				echo "There are no unarchived posts."
				exit 0
			fi
			;;

	-v|--version)
			echo "${PHARC_VERSION}"
			exit 0
			;;

	-y|--year)
			echo "$(date +'%Y')"
			exit 0
			;;

	*)	Usage
			exit 1

	esac
fi

# "You can see the output of this script visiting my gopher hole at:"
# "<gopher://sdfeu.org/1/users/chals/>"

exit 0
