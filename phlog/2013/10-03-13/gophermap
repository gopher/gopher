schroot
Thursday Oct  3  9:22:41 2013
------------------------------

During this week I've spent some time looking for a script to create 
a chroot automatically, with minimal manual intervention. I found two
very nice looking scripts but much to my surprise they both mentioned
an incredibly handy tool called "schroot".

At first I didn't give it much credit, there are so many programs out
there and they are all so useful for some people, but it is not very 
likely that all software is useful for all people. Such is life! 

In the end I decided to give it a try. And guess what? It really left
me stunned. Yeah, believe it or not I constantly use chroots for tests
always as root, and as you can imagine, after all the testing is done,
the chroots usually end up in a nearly unusable state. Then what I do 
is building another chroot, hence my quest for a script to automatize
part of the process.

Well, what "schroot" does is allowing you to enter a chroot without 
root privileges, and what is even better, all changes disappear after 
you leave the chroot (unless you want to keep the session open for 
a later time, or another day...)

All you need to use schroot is a config file /etc/schroot/schroot.conf
which can be really simple. Mine looks like this:

[sid]
description=Debian sid (unstable)
directory=/sid
users=chals

The description line is optional, the directory is the location of 
the chroot and the users= field is your username.

Then simply open a terminal and type:

 $ schroot -c sid

And there you are. You are a regular user inside the chroot. 
Have fun!
