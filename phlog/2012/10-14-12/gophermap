elinks, the full featured text browser
Sunday Oct 14 10:21:44 2012
------------------------------

Text browsers are absolutely necessary to surf the web in
non-graphical environments. However, many of them have been 
unmaintained for several years so I fear that some of them might
disappear in the long run. 

The first text browsers I used under a gnu/linux distribution were
w3m and lynx. I discovered elinks much later but I deeply 
fell in love with it at first sight. 

elinks is a real personal favourite. Even though I must confess that
I mainly use w3m and lynx in my local machines. This has an
explanation, do not start freaking out yet!

When I have to translate, I use wordreference.com a lot. The adds are
so annoying that I end up logging into my shell account at 
sdf-eu and visit wordreference.com with the help of elinks. Man, that
is a different world: speed, no distractions at all and besides, you
almost have the same features you have with a regular graphical
browser.

Whereas I deeply despise adds and flashy stuff when I'm looking for
information. I do want to see the pictures if I'm visiting 
any of my friends' personal websites. That is why I use w3m at home.
You install w3m-img and you get instant inline image 
support. So it is really for that reason. I use lynx at home too
because it is the only one with out-of-the-box support for 
gopher.

Does that mean that you cannot see images or visit gopher holes with
elinks? Not at all, with elinks I can do it all and much more. 
I can check my gmail account, log into libre.fm to see my listening
stats and so many things that I can't possibly name them 
all. It would be easier to make a list of the few things that I can't
do.

The gopher support in elinks is a compile time option. You only need
to compile it with gopher support and there you go. In 
order to see images you only need an external image viewer and tell
elinks to use it. When you select an image elinks will prompt you
what to do with it. You only need to type which external program you
want to use and add % which will be substituted with the image file
and voila! the image is there.

Typing the name of the image viewer every time can be a pita so you
can add to ~/.elinks/elinks.conf something like this:

set mime.extension.jpg="image/jpeg"
set mime.extension.jpeg="image/jpeg"
set mime.extension.png="image/png"
set mime.extension.gif="image/gif"
set mime.extension.bmp="image/bmp"

set mime.handler.image_viewer.unix.ask = 0
set mime.handler.image_viewer.unix.block = 0
set mime.handler.image_viewer.unix.program = "vp %"

set mime.type.image.jpg = "image_viewer"
set mime.type.image.jpeg = "image_viewer"
set mime.type.image.png = "image_viewer"
set mime.type.image.gif = "image_viewer"
set mime.type.image.bmp = "image_viewer"


In my shell account at sdf-eu, elinks has already got gopher support
enabled. I simply have to add the previous mime types and 
handlers to the right location. If you have not noticed it yet, I use
'vp' to view the images and huh I almost forget!!! 
Enabling colours is a must. To do that, press the Esc key while
running elinks. Go to the menu bar that appears at the top, 
select Setup and Terminal options and there select either 16, 88 or
256 colours. And then just, Enjoy!!!
